#include "colors.inc"

camera {
    location <0,0,-50>
    look_at <0,0,0>    
    angle 15
}

light_source {
    <4,6,-10>, White
}

sky_sphere {
    pigment {
        gradient y
        color_map {
            [0 color White]
            [1 color Blue]
        }
        
        scale 2 
        translate <0,1,0>
    }
}

cylinder {
    <0,-2,0>, <0,1,0>, 1 
    open
    
    pigment {
        color Green 
    }
    
    finish {
        ambient .4
    } 
    
    rotate <-30,0,0>  
    
    translate <3.9,3.1,0> 
         
}

box {
    <-1,-2,-1>, <0.5,1.5,0.5>
  
    pigment {
        color Magenta
    }
    
    finish {
        ambient .4
    }
    
    rotate <-30,20,0>
    
    translate <-4,3,0>    
}

cone {
    <0,-3,0>, 1, <0,0,0>, 0
    
    pigment{
        color Blue
    }
    
    finish {
        ambient .4
    }
    
    rotate <-30,0,0> 
    
    translate <-5,-1,0>       
}

torus {
    3.2,0.8
    pigment {
        color Red
    }
    
    finish {
        ambient .4
    }
    
    rotate <-30,0,0>
    
    translate <1,-1.5,0>        
} 

                 