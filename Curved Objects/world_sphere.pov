#include "colors.inc"

camera {
    location <5, 2, -15>
    look_at <0, 0, 0>
    angle 15
}

light_source {
    <4, 6, -10>, White
    //shadowless
}

sky_sphere {
    pigment {
        gradient y
        color_map {
            [0 color White]
            [0.5 color Blue] 
            [1 color Black]
        }
        
        //scale 2 
        translate <0, .001, 0>
    }
}


sphere {
    <0,0,0>, 0.7
    
    pigment {
        image_map { png "world.JPG" map_type 1 }
    }
    finish { ambient .4 }
    translate <0.6, 0, 0>
}