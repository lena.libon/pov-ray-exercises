#include "colors.inc"

camera {
    location <5, 2, -15>
    look_at <.5, .5, 0>
    angle 15
}

light_source {
    <4, 6, -10>, White
    //shadowless
}

sky_sphere {
    pigment {
        gradient y
        color_map {
            [0 color Magenta]
            [1 color Blue]
        }
        
        //scale 2 
        translate <0, .3, 0>
    }
}

box {
    <0, 0, 0>, <1, 1, 1>
    pigment {
        image_map { jpeg "picture.JPG" }
    }
    finish {ambient .4}
    
    translate <-1.5, 0, 0>                  
    
    scale <620, 827, 620>/1000
    
    scale 1.5
    
}

cylinder {
   <.5, 0, 0>, <.5, 1, 0>, .5 
   open 
   pigment {
    image_map { 
        jpeg "picture.JPG"
        // map_type 1
    }
   }
   finish {ambient .4}
   
   translate <.5, 0, 0> 
   
   scale <620, 827, 620>/1000
   
   scale 1.5 
}

                                 